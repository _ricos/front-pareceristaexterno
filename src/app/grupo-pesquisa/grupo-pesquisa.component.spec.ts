import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrupoPesquisaComponent } from './grupo-pesquisa.component';

describe('GrupoPesquisaComponent', () => {
  let component: GrupoPesquisaComponent;
  let fixture: ComponentFixture<GrupoPesquisaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrupoPesquisaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrupoPesquisaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
