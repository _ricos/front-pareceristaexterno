// Import of libs
import { Component, OnInit } from '@angular/core';

import { AppService } from '../app.service';
import { HomeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  gridActive: boolean = true;
  qtdProjetos: any;
  _id: any;
  
  constructor(
    public _homeService: HomeService, public _appService: AppService) { 
      this._id = this._appService.idExterno;
    }

  ngOnInit() {
    this.retornaqtdProjetos();

  }

  retornaqtdProjetos(){
    this._homeService.retornaQtdProjeto(this._id).subscribe((data : any) => {      
      this.qtdProjetos = data;
      // this._appService.hideProgressBar();
    }, (error: any) => {
      // this._appService.hideProgressBar();
      console.log(error);
    })
  }
}
