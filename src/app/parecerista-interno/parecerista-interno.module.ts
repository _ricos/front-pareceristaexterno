import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { PareceristaInternoComponent } from './parecerista-interno.component';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  declarations: [PareceristaInternoComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule,
    NgxMaskModule.forChild()
  ]
})
export class PareceristaInternoModule { }
