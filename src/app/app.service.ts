import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup } from '@angular/forms';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { NgProgress } from '@ngx-progressbar/core';

@Injectable()
export class AppService {

  componentRef: any; // Reference to destroy element
  elementRef: any; // receive the element
  isLogged: boolean = false;
  numeroFuncional: any = this.getUrlParameter('funcional', location.href) || localStorage.getItem('numeroFuncional');
  usuarioExterno: any = JSON.parse(localStorage.getItem('usuarioExterno'));
  idExterno: any = this.idExterno == undefined && this.isLogged ? JSON.parse(localStorage.getItem('usuarioExterno')).Id : this.idExterno;
  private windowWidth;
  public loaderIsActive: boolean = false;
  headerOptions;


  constructor(
    public toastr: ToastrService,
    public ngProgress: NgProgress,
    public _http: HttpClient
  ) {
    this.headerOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded'
      })
    }

    if(this.numeroFuncional != null) {
      localStorage.setItem('numeroFuncional', this.numeroFuncional);
    }
  }

  getCookie(name) {
    let value = "; " + document.cookie;
    let parts = value.split(`; ${name}=`);
    if (parts.length == 2) return parts.pop().split(";").shift();
}

  tratarObjeto(obj) {
    var newObj = '';
    let array = Object.getOwnPropertyNames(obj);
    $.each(array, (index, value) => { newObj += value + '=' + obj[value] + "&" })
    return newObj;
  }

  showToastrSuccess(msg: string) {
    this.toastr.success(msg);
  }

  showToastrWarning(msg: string) {
    this.toastr.warning(msg);
  }

  showToastrError(msg: string) {
    this.toastr.error(msg);
  }

  normalizarString(a: string) {
    return a = a = a.replace(/[EÉÈÊË]/gi, "E"),
    a = a.replace(/[eéèêë]/gi, "e"),        
    a = a.replace(/[AÀÁÂÃÄÅÆ]/gi, "A"),
    a = a.replace(/[aàáãâä]/gi, "a"),
    a = a.replace(/[cç]/gi, "c"),
    a = a.replace(/[IÌÍÎÏ]/gi, "I"),
    a = a.replace(/[iíìïî]/gi, "i"),
    a = a.replace(/[ÒÓÔÕÖ]/gi, "O"),
    a = a.replace(/[oóòôö]/gi, "o"),
    a = a.replace(/[UÜÛÙÚ]/gi, "U"),
    a = a.replace(/[uúùüû]/gi, "u")        
  }

  verificaValidacoesForm(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((campo) => {
        var controle = formGroup.get(campo);
        controle.markAsTouched();
        if (controle instanceof FormGroup) {
            this.verificaValidacoesForm(controle);
        }
    });
  }

  getRandomNumber(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  isDesktop() {
    this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    return this.windowWidth >= 768 ? true : false;
  }

  isMobile() {
    this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    return this.windowWidth < 768 ? true : false;
  }

  getUrlParameter(name, url) {
    if (!url) url = location.href;
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( url );
    return results == null ? null : results[1];
  }

  showProgressBar() {       
    this.ngProgress.start();
    this.loaderIsActive = true;
  }

  hideProgressBar() {
    if(this.loaderIsActive) 
      this.ngProgress.done();
      this.loaderIsActive = false;        
  }

  createCookie(cookieName, cookieValue) {
    let now: any = new Date();
    let time = now.getTime();
    let expireTime = time + 1000 * 36000;
    now.setTime(expireTime);
    document.cookie = `${cookieName}=${cookieValue};expires=${now.toGMTString()};path=/`;
  }

  removeCookie( name ) {
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  }

  buscarCep(_cep : string){
    return this._http.get(`$https://viacep.com.br/ws/06213005/json/`,this.headerOptions);
  }

  //Set cookies
  // this.cookieService.set('cookieApp', 'Welcome you, Anil!' );
  //Get Cookies
  // this.cookieValue = this.cookieService.get('cookieApp');

  // //Check Cookies
  // const IsCookieExists: boolean = this.cookieService.check('cookieApp');
  // //Get Cookies
  // const value: string = this.cookieService.get('cookieApp');
  // //Get all cookies
  // const allCookies: {} = this.cookieService.getAll();
  // //delete cookies
  // this.cookieService.delete('cookieApp');
  // //delete all cookies
  // this.cookieService.deleteAll();
}
