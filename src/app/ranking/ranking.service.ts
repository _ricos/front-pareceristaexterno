import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from "rxjs/operators";
import { baseUrl } from '../shared/shared.variables';
import { AppService } from '../app.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RankingService {

  constructor(
    public _http: HttpClient,
    public appService: AppService
  ) { }

  listarProjetosRanking(id):any {
    return this._http.get(`${baseUrl}ProjetoPesquisa/ProjetosPareceristaExterno?id=${id}`,this.appService.headerOptions);
  }
}