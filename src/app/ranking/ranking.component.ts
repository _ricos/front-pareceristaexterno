import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { RankingService } from './ranking.service';
import { AppService } from '../app.service';
import { baseUrl } from '../shared/shared.variables';
import { VisualizacaoProjetoService } from '../lista-projetos/visualizacao-projeto/visualizacao-projeto.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss']
})
export class RankingComponent implements OnInit {
  baseUrl: string = baseUrl;
  aluno: any;
  lstProjetos: any;
  resumo: any;
  detalhe: any;
  pareceristaInterno: any;


  bloqueioHistorico: boolean = false;
  historicoCarregado: boolean;
  classificacaoRealizada: boolean;
  existeProjeto: boolean;

  constructor(
    public _rankingService: RankingService,
    public route: ActivatedRoute,
    public router: Router,
    public appService: AppService,
    public _visualizacaoProjetoService: VisualizacaoProjetoService,
  ) {
  }

  ngOnInit() {
    this.listarRanking();
  }

  listarRanking() {
    this._rankingService.listarProjetosRanking(this.appService.idExterno).subscribe((data: any) => {
      this.lstProjetos = data;

      this.existeProjeto = this.lstProjetos.length > 0;

    }, err => {
      this.appService.showToastrWarning("Não foi possível localizar nenhum projeto.");
    });
  }

  carregarResumo(resumo: any) {
    this.resumo = resumo;
  }

  carregarDetalhe(_detalhe: any) {
    this.detalhe = _detalhe;
    this.carregarParecerista();
  }

  retornaHistorico() {
    this._visualizacaoProjetoService.retornaHistoricoEscolar(this.detalhe.Ra)
      .subscribe((data: any) => {
        this.aluno = data;
        this.historicoCarregado = true;
        this.bloqueioHistorico = false;
      }, (error: any) => {
        this.historicoCarregado = true;
        this.bloqueioHistorico = true;
        this.appService.showToastrError("Não foi possível localizar seu histórico escolar.");
        return;
      });
  }

  carregarParecerista() {
    this._visualizacaoProjetoService.retornarPareceristaInterno(this.detalhe.PareceristaInterno)
      .subscribe((data: any) => {
        this.pareceristaInterno = data;
        this.retornaHistorico();

      }, (error: any) => {
        this.appService.showToastrError("Não foi possível localizar o parecerista do projeto.");
        return;
      });
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.lstProjetos, event.previousIndex, event.currentIndex);
  }

  verificarClassificaao() {
    this._visualizacaoProjetoService.retornarPareceristaInterno(this.detalhe.PareceristaInterno)
      .subscribe((data: any) => {

        this.classificacaoRealizada = data;

      }, (error: any) => {
        this.appService.showToastrError("Não foi possível localizar o parecerista do projeto.");
        return;
      });
  }

  classificarProjetos() {
    let myArray: any = [];
    let i = 1;

    this.lstProjetos.forEach(function (value) {
    
        myArray.push({
            Protocolo: value.Protocolo,
            Ranking: i++
        })
    });

    let body = {
      IdPareceristaExterno: this.appService.idExterno,
      Projetos: myArray
    }

    $.ajax({
      url: `${baseUrl}Ranking/InserirRankingExterno`,
      method: "POST",
      data: body,
      success: (data) => {
        this.appService.showToastrSuccess("Classificação cadastrada com sucesso.");
      },
      error: (error) => {
        console.log(error);
        this.appService.showToastrError("Erro ao cadastrar a classificação.");
      }
    });
  }
}