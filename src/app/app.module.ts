// Import of libs
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import * as $ from 'jquery';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpClientModule } from '@ngx-progressbar/http-client';

// Import of modules, components and services
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { SharedModule } from './shared/shared.module';
import { HomeModule } from './home/home.module';
import { AppService } from './app.service';
import { HttpClientModule } from '@angular/common/http';
import { HomeService } from './home/home.service';
import { LoginModule } from './login/login.module';
import { LoginService } from './login/login.service';
import { ListaProjetosModule } from './lista-projetos/lista-projetos.module';
import { ListaProjetosService } from './lista-projetos/lista-projetos.service';
import { RouterModule } from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';
import { OrientadorDadosModule } from './orientador-dados/orientador-dados.module';
import { OrientadorDadosService } from './orientador-dados/orientador-dados.service';
import { PareceristaInternoService } from './parecerista-interno/parecerista-interno.service';
import { PareceristaInternoModule } from './parecerista-interno/parecerista-interno.module';
import { PareceristaExternoModule } from './parecerista-externo/parecerista-externo.module';
import { PareceristaExternoService } from './parecerista-externo/parecerista-externo.service';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule, NgbPaginationModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { GrupoPesquisaComponent } from './grupo-pesquisa/grupo-pesquisa.component';
import { GrupoPesquisaService } from './grupo-pesquisa/grupo-pesquisa.service';
import { GrupoPesquisaModule } from './grupo-pesquisa/grupo-pesquisa.module';
import { CookieService } from 'ngx-cookie-service';
import { RankingModule } from './ranking/ranking.module';
import { RankingService } from './ranking/ranking.service';
import { RankingExternoModule } from './ranking-externo/ranking-externo.module';
import {NgxMaskModule} from 'ngx-mask';
import { AccessNotAllowedComponent } from './access-not-allowed/access-not-allowed.component';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    AccessNotAllowedComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    NgbModule,
    NgbPaginationModule,
    NgbAlertModule,

    // Import modules of application
    SharedModule,
    LoginModule,
    HomeModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    ListaProjetosModule,
    OrientadorDadosModule,
    PareceristaInternoModule,
    PareceristaExternoModule,
    GrupoPesquisaModule,
    RankingModule,
    RankingExternoModule,
    NgProgressModule.forRoot(),
    NgProgressHttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot()
  ],
  exports: [],
  providers: [
    AppService,
    LoginService,
    HomeService,
    ListaProjetosService,
    OrientadorDadosService,
    PareceristaInternoService,
    PareceristaExternoService,
    GrupoPesquisaService,
    RankingService,
    RankingService,
    CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
