import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from "rxjs/operators";
import { baseUrl } from '../shared/shared.variables';
import { AppService } from '../app.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RankingExternoService {

  constructor(
    public _http: HttpClient,
    public appService: AppService
  ) { }

  listarProjetosRanking() {
    return this._http.get(`${baseUrl}ProjetoPesquisa/ListarRanking`, this.appService.headerOptions);
  }
}