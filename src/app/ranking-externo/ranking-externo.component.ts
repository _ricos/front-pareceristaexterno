import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { RankingExternoService } from './ranking-externo.service';
import { AppService } from '../app.service';
import { baseUrl } from '../shared/shared.variables';
import { VisualizacaoProjetoService } from '../lista-projetos/visualizacao-projeto/visualizacao-projeto.service';

@Component({
  selector: 'app-ranking-externo',
  templateUrl: './ranking-externo.component.html',
  styleUrls: ['./ranking-externo.component.scss']
})
export class RankingExternoComponent implements OnInit {
  baseUrl: string = baseUrl;
  aluno: any;
  lstProjetos: any;
  resumo: any;
  detalhe: any;
  pareceristaInterno: any;

  bloqueioHistorico: boolean = false;
  historicoCarregado: boolean;

  constructor(
    public _rankingService: RankingExternoService,
    public route: ActivatedRoute,
    public router: Router,
    public appService: AppService,
    public _visualizacaoProjetoService: VisualizacaoProjetoService,
  ) {
  }

  ngOnInit() { 
    this.listarRanking();
  }

  listarRanking(){
    this._rankingService.listarProjetosRanking().subscribe((data: any) => {
      this.lstProjetos = data;

    }, err => {
      this.appService.showToastrWarning("Não foi possível localizar nenhum projeto.");
    });
  }
  
  carregarResumo(resumo: any){
    this.resumo = resumo;
  }

  carregarDetalhe(_detalhe: any){
    this.detalhe = _detalhe;
    this.carregarParecerista();
  }

  retornaHistorico() {
    this._visualizacaoProjetoService.retornaHistoricoEscolar(this.detalhe.Ra)
      .subscribe((data: any) => {
        this.aluno = data;
        this.historicoCarregado = true;
        this.bloqueioHistorico = false;
      }, (error: any) => {
        this.historicoCarregado = true;
        this.bloqueioHistorico = true;
        this.appService.showToastrError("Não foi possível localizar seu histórico escolar.");
        return;
      });
  }

  carregarParecerista() {
    this._visualizacaoProjetoService.retornarPareceristaInterno(this.detalhe.PareceristaInterno)
      .subscribe((data: any) => {
        this.pareceristaInterno = data;
        this.retornaHistorico();
       
      }, (error: any) => {
        this.appService.showToastrError("Não foi possível localizar o parecerista do projeto.");
        return;
      });
  }
}
