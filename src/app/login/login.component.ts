import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { LoginService } from './login.service';
import { AppService } from '../app.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formLogin: FormGroup;
  verifyLoader: boolean;

  constructor(
    public _loginService: LoginService,
    public router: Router,
    public appService: AppService
  ) {
    this.formLogin = new FormGroup({
      usuario: new FormControl('', [Validators.required, Validators.nullValidator, Validators.email]),
      senha: new FormControl('', [Validators.required, Validators.nullValidator, Validators.minLength(8), Validators.maxLength(20)])
    });
  }

  ngOnInit() { }

  doLogin() {
    if (!this.formLogin.valid) {
      this.appService.showToastrError(`E-mail e Senha devem ser preenchidos corretamente.`);
      return;
    }

    let body = this.formLogin.value;
    this.verifyLoader = true;
    this._loginService.authetication(body)
      .subscribe(
        (res: any) => {
          console.log(res);
          this.verifyLoader = !this.verifyLoader;
          if (res != null) {
            localStorage.setItem('usuarioExterno', JSON.stringify(res));
            this.appService.createCookie("usuarioExterno", true);
            this.appService.isLogged = true;
            this.appService.idExterno = res.Id;
            this.router.navigate(["/home"]);
          } else {
            localStorage.removeItem('usuarioExterno');
            this.appService.removeCookie("usuarioExterno");
            this.appService.isLogged = false;
            this.appService.showToastrError(res.Mensagem);
          }

        },
        err => {
          if (err.status == 401) {
            this.verifyLoader = !this.verifyLoader;
            this.appService.showToastrError(`Usuário ou Senha inválidos.`);
            return;
          }

          console.log(err);
          this.verifyLoader = !this.verifyLoader;
          this.appService.showToastrError(`Ocorreu algum erro inesperado`);
        }
      );
  }

}
