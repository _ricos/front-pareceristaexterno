// Import of libs
import { Component, OnInit } from '@angular/core';

import { AppService } from '../app.service';
import { ListaProjetosService } from './lista-projetos.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';


@Component({
  selector: 'app-lista-projetos',
  templateUrl: './lista-projetos.component.html',
  styleUrls: ['./lista-projetos.component.scss']
})
export class ListaProjetosComponent implements OnInit {
  [x: string]: any;

  p: number = 1;
  qtdProjetos : number;
  listaProjeto : any;

  constructor(
    public _appService: AppService,
    public _listaProjetosService: ListaProjetosService,
    public route: ActivatedRoute,
    public router: Router
  ) { }

  ngOnInit() {
    this.requestProjects();

    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (this.route.children.length === 0) {
          this.requestProjects();
        }
      }
    });
  }

  requestProjects() {
    this._listaProjetosService.listaProjeto = [];
    this._listaProjetosService.listaProjetos().subscribe(
      (data: any) => {
        if(data.length == 0){
          this._appService.showToastrWarning("Nenhum projeto encontrado.");
          return;
        }
        this._listaProjetosService.listaProjeto = data;
    }, (error: any) => {
          this._appService.showToastrError("Não foi possível realizar a busca de projetos.");
          return;
    });
  }
}
