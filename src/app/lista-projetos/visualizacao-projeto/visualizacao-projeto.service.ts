import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppService } from '../../app.service';
import { baseUrl } from '../../shared/shared.variables';

@Injectable()
export class VisualizacaoProjetoService {

  constructor(
    private _http: HttpClient,
    public appService: AppService
  ) { }

  retornaOrientador(_numeroFuncional) {
    return this._http.get(`${baseUrl}Professor/GetProfessorByNumeroFuncional?numeroFuncional=${_numeroFuncional}`,this.appService.headerOptions);
  }

  getProjetoPesquisa(protocolo: any) {
    return this._http.get(`${baseUrl}ProjetoPesquisa/Retornar?protocolo=${protocolo}`, this.appService.headerOptions);
  }

  retornaHistoricoEscolar(ra: any) {
    return this._http.get(`${baseUrl}ProjetoPesquisa/Historico?ra=${ra}`, this.appService.headerOptions);
  }

  retornaParecerOrientador(protocolo: any) {
    return this._http.get(`${baseUrl}ParecerOrientadorAluno/Retornar?protocolo=${protocolo}`, this.appService.headerOptions);
  }

  retornarPareceristaInterno(numeroFuncional: any) {
    return this._http.get(`${baseUrl}PareceristaInterno/RetornaPareceristaInterno?NumeroFuncional=${numeroFuncional}`
    , this.appService.headerOptions);

  }
  postComentario(obj: any) {
    return this._http.post(`${baseUrl}ProjetoPesquisa/InsereObsSetorPesquisa`, this.appService.tratarObjeto(obj)
    , this.appService.headerOptions);
  }

  postPareceristaInterno(obj: any) {
    return this._http.post(`${baseUrl}ProjetoPesquisa/DefinePareceristaInterno`
    , this.appService.tratarObjeto(obj), this.appService.headerOptions);
  }

  alteraOrientadorProjeto(obj: any) {
    return this._http.post(`${baseUrl}ProjetoPesquisa/DefineOrientador`, this.appService.tratarObjeto(obj), this.appService.headerOptions);
  }

  retornaParecerParecerista(protocolo: any) {
    return this._http.get(`${baseUrl}ProjetoPesquisa/RespostaPareceristaInterno?protocolo=${protocolo}`, this.appService.headerOptions);
  }
}
