// Import of libs
import { Component, OnInit } from '@angular/core';

import { AppService } from '../../app.service';
import { VisualizacaoProjetoService } from './visualizacao-projeto.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { baseUrl } from '../../shared/shared.variables';

@Component({
  selector: 'app-visualizacao-projeto',
  templateUrl: './visualizacao-projeto.component.html',
  styleUrls: ['./visualizacao-projeto.component.scss']
})
export class VisualizacaoProjetoComponent implements OnInit {
  baseUrl: string = baseUrl;

  protocolo: number;
  projeto: any;
  aluno: any;
  orientador: any;
  parecerOrientador : any;
  funcionalOrientadorProjeto: any;
  pareceristaInterno: any;
  lstParecerParecerista: any;
  
  projetoCarregado: boolean;
  bloqueioSetorPesquisa: boolean = true;
  projetoValidadoOrientador: boolean;
  pareceristaInternoAssociado: boolean; //Verificar se o parecerista interno já validou o projeto para travar.
  historicoCarregado: boolean;
  bloqueioHistorico: boolean = false;
  parecerConcluido: boolean;

  formObsSetorPesquisa: FormGroup;
  obsSetorPesquisa = new FormControl('');
  funcionalOrientador = new FormControl('');
  funcionalParecerista = new FormControl('');

  constructor(
    public _appService: AppService,
    public _visualizacaoProjetoService: VisualizacaoProjetoService,
    public route: ActivatedRoute,
  ) {
    this.route.params.subscribe((params: any) => {
      this.protocolo = params['id'];
    });

    this.formObsSetorPesquisa = new FormGroup({
      obsSetorPesquisa: new FormControl('')
    });
  }

  ngOnInit() {
    this.retornaProjetoPesquisa();
    this.retornaParecerOrientador();
  }

  retornaProjetoPesquisa() {
    this._visualizacaoProjetoService.getProjetoPesquisa(this.protocolo)
      .subscribe((data: any) => {
        this.projeto = data;
        this.formObsSetorPesquisa.setValue({ obsSetorPesquisa : this.projeto.Observacao});
        this.projetoCarregado = true;
        this.pareceristaInternoAssociado = data.PareceristaInterno != ''
        if(this.pareceristaInternoAssociado)
          this.retornaParecerParecerista();
          
        this.retornaHistorico();
      }, (error: any) => {
        this._appService.showToastrError("Não foi possível realizar a busca do projeto.");
        return;
      });
  }

  retornaParecerOrientador() {
    this._visualizacaoProjetoService.retornaParecerOrientador(this.protocolo)
      .subscribe((data: any) => {
        this.parecerOrientador = data.Parecer;
        this.projetoValidadoOrientador = data.Validado;
        this.funcionalOrientadorProjeto = data.NumeroFuncional
      }, (error: any) => {
        this._appService.showToastrError("Não foi possível retornar o parecer do orientador.");
        return;
      });
  }

  retornaHistorico() {
    this._visualizacaoProjetoService.retornaHistoricoEscolar(this.projeto.Ra)
      .subscribe((data: any) => {
        this.aluno = data;
        this.historicoCarregado = true;
        this.bloqueioHistorico = false;
      }, (error: any) => {
        this.historicoCarregado = true;
        this.bloqueioHistorico = true;
        this._appService.showToastrError("Não foi possível localizar seu histórico escolar.");
        return;
      });
  }

  retornaParecerParecerista() {
    this._visualizacaoProjetoService.retornaParecerParecerista(this.protocolo)
      .subscribe((data: any) => {
        this.lstParecerParecerista = data;
        this.parecerConcluido = data.length > 0
      }, (error: any) => {
        this._appService.showToastrError("Não foi possível retornar o parecer do parecerista interno.");
        return;
      });
  }

  pesquisarOrientador() {
    let _func = this.funcionalOrientador.value;
    
    if (_func == '' || _func == undefined || _func.length <= 5) {
      this.orientador = null;
      this._appService.showToastrWarning(`Preencha os campos corretamente.`);
      return;
    }

    this._visualizacaoProjetoService.retornaOrientador(_func).subscribe((data: any) => {
      if (data.NumeroFuncional == undefined || data.NumeroFuncional == '') {
        this._appService.showToastrWarning(`Orientador não encontrado.`);
        this.orientador = null;
        return;
      }
      this.orientador = data;
    },
      err => {
        this.orientador = null;
        this._appService.showToastrError(`Ocorreu algum erro inesperado.`);
      });
  }

  retornarPareceristaInterno() {
    let _func = this.funcionalParecerista.value;
    
    if (_func == '' || _func == undefined || _func.length <= 5) {
      this.pareceristaInterno = null;
      this._appService.showToastrWarning(`Preencha os campos corretamente.`);
      return;
    }

    this._visualizacaoProjetoService.retornarPareceristaInterno(_func).subscribe((data: any) => {
      if (data.NumeroFuncional == undefined || data.NumeroFuncional == '') {
        this._appService.showToastrWarning(`Parecerista interno não encontrado.`);
        this.pareceristaInterno = null;
        return;
      }

      if(this.funcionalOrientadorProjeto == data.NumeroFuncional)
      {
        this._appService.showToastrWarning(`O parecerista interno, não pode ser o mesmo que o orientador.`);
        return;
      }

      this.pareceristaInterno = data;
    },
      err => {
        this.pareceristaInterno = null;
        this._appService.showToastrError(`Ocorreu algum erro inesperado.`);
      });
  }

  alterarOrientadorProjeto() {
    let body = {
      Protocolo : this.protocolo,
      NumeroFuncional : this.orientador.NumeroFuncional,
    }

    this._visualizacaoProjetoService.alteraOrientadorProjeto(body)
      .subscribe((data: any) => {
        if (data) {
          this._appService.showToastrSuccess(`Orientador alterado com sucesso!`);
          this.retornaProjetoPesquisa();
        } else {
          this._appService.showToastrError(`Não foi possível alterar o orientador.`);
        }
      },
      err => {
        this._appService.showToastrError(`Ocorreu algum erro inesperado.`);
      });
  }

  enviarComentario() {
    let body = {
      Protocolo: this.protocolo,
      Comentario: this.formObsSetorPesquisa.value.obsSetorPesquisa
    }

    this._visualizacaoProjetoService.postComentario(body).subscribe((data: any) => {
     
      if (data) {

        if(this.pareceristaInterno != null)
          this.vincularPareceristaInterno();

        this._appService.showToastrSuccess(`Informações salvas com sucesso!`);
        
      } else {
        this._appService.showToastrError(`Não foi possível salvar a observação.`);
      }
    },
      err => {
        this._appService.showToastrError(`Ocorreu algum erro inesperado.`);
      });
  }

  vincularPareceristaInterno(){
    let body = {
      Protocolo : this.protocolo,
      NumeroFuncional : this.pareceristaInterno.NumeroFuncional,
    }
    
    this._visualizacaoProjetoService.postPareceristaInterno(body).subscribe((data: any) => {

      this.retornaProjetoPesquisa();
      },
      err => {
        this._appService.showToastrError(`Ocorreu algum erro inesperado.`);
      });
  }
}
