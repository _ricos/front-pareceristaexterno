
// Imports of libs
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../../app.service';
import { CookieService } from 'ngx-cookie-service';

// Import of modules, components and services

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  usuario: any;

  constructor(public router: Router, public appService: AppService, private cookieService: CookieService ) { }

  ngOnInit() {
    this.usuario = JSON.parse(localStorage.getItem("usuarioExterno"));
  }

  logout() {
    localStorage.removeItem('usuarioExterno');
    this.appService.removeCookie("usuarioExterno");
    this.cookieService.delete('usuarioExterno');
    this.cookieService.deleteAll("usuarioExterno");
    this.appService.isLogged = false;
    this.router.navigate(["/login"]);
    return;
  }
}
