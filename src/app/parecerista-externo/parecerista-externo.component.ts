import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PareceristaExternoService } from "./parecerista-externo.service";
import { AppService } from '../app.service';

@Component({
  selector: 'app-parecerista-externo',
  templateUrl: './parecerista-externo.component.html',
  styleUrls: ['./parecerista-externo.component.scss']
})
export class PareceristaExternoComponent implements OnInit {

  _token: string;
  pareceristaExterno: any;
  ufs: any;


  emailValido: boolean;
  cadastroConcluido: boolean;
  senhaValida: boolean;
  datasValidadas: boolean;
  cpfValido: boolean;

  formCadastraParecerista: FormGroup;

  id = new FormControl('');
  token = new FormControl('');
  nome = new FormControl('');
  email = new FormControl('');
  sexo = new FormControl('');
  rg = new FormControl('');
  cpf = new FormControl('');

  pisPasep = new FormControl('');

  telefone = new FormControl('');
  celular = new FormControl('');

  estado = new FormControl('');
  cidade = new FormControl('');
  bairro = new FormControl('');
  cep = new FormControl('');
  endereco = new FormControl('');
  complemento = new FormControl('');

  unidade = new FormControl('');
  curso = new FormControl('');
  departamento = new FormControl('');

  mestradoInicio = new FormControl('');
  mestradoConclusao = new FormControl('');
  mestradoInstituicao = new FormControl('');
  mestradoDepartamento = new FormControl('');
  mestradoOrientador = new FormControl('');
  tituloDissertacao = new FormControl('');

  doutoradoInicio = new FormControl('');
  doutoradoConclusao = new FormControl('');
  doutoradoInstituicao = new FormControl('');
  doutoradoDepartamento = new FormControl('');
  doutoradoOrientador = new FormControl('');
  tituloTese = new FormControl('');

  area1 = new FormControl('');
  area2 = new FormControl('');
  area3 = new FormControl('');
  area4 = new FormControl('');

  banco = new FormControl('');
  codigoBanco = new FormControl('');
  codigoAgencia = new FormControl('');
  NumeroConta = new FormControl('');

  senha = new FormControl('');
  confirmarSenha = new FormControl('');

  constructor(
    public _pareceristaExternoService: PareceristaExternoService,
    public router: Router,
    public route: ActivatedRoute,
    public appService: AppService
  ) {
    this.formCadastraParecerista = new FormGroup({

      id: new FormControl(''),
      token: new FormControl(''),
      nome: new FormControl('', [Validators.required, Validators.nullValidator, Validators.minLength(5)]),
      email: new FormControl('', [Validators.required, Validators.email, Validators.nullValidator, Validators.minLength(5)]),
      sexo: new FormControl(''),
      rg: new FormControl('', [Validators.required, Validators.nullValidator, Validators.minLength(5)]),
      cpf: new FormControl('', [Validators.required, Validators.nullValidator, Validators.minLength(5)]),

      pisPasep: new FormControl('', [Validators.required, Validators.nullValidator, Validators.minLength(3)]),

      telefone: new FormControl(''),
      celular: new FormControl('', [Validators.required, Validators.nullValidator, Validators.minLength(5)]),

      estado: new FormControl('', [Validators.required, Validators.nullValidator]),
      cidade: new FormControl('', [Validators.required, Validators.nullValidator]),
      bairro: new FormControl('', [Validators.required, Validators.nullValidator]),
      cep: new FormControl('', [Validators.required, Validators.nullValidator, Validators.minLength(5)]),
      endereco: new FormControl('', [Validators.required, Validators.nullValidator, Validators.minLength(5)]),
      complemento: new FormControl(''),

      unidade: new FormControl(''),
      curso: new FormControl(''),
      departamento: new FormControl(''),

      mestradoInicio: new FormControl('', [Validators.required, Validators.nullValidator, Validators.minLength(3)]),
      mestradoConclusao: new FormControl('', [Validators.required, Validators.nullValidator, Validators.minLength(3)]),
      mestradoInstituicao: new FormControl(''),
      mestradoDepartamento: new FormControl(''),
      mestradoOrientador: new FormControl(''),
      tituloDissertacao: new FormControl(''),

      doutoradoInicio: new FormControl('', [Validators.required, Validators.nullValidator, Validators.minLength(3)]),
      doutoradoConclusao: new FormControl('', [Validators.required, Validators.nullValidator, Validators.minLength(3)]),
      doutoradoInstituicao: new FormControl(''),
      doutoradoDepartamento: new FormControl(''),
      doutoradoOrientador: new FormControl(''),
      tituloTese: new FormControl(''),

      area1: new FormControl(''),
      area2: new FormControl(''),
      area3: new FormControl(''),
      area4: new FormControl(''),

      banco: new FormControl('', [Validators.required, Validators.nullValidator]),
      codigoBanco: new FormControl('', [Validators.required, Validators.nullValidator]),
      codigoAgencia: new FormControl('', [Validators.required, Validators.nullValidator]),
      NumeroConta: new FormControl('', [Validators.required, Validators.nullValidator]),
      senha: new FormControl('', [Validators.required, Validators.nullValidator, Validators.minLength(8), Validators.maxLength(20)]),
      confirmarSenha: new FormControl('', [Validators.required, Validators.nullValidator, Validators.minLength(8), Validators.maxLength(20)]),
    });
  }

  ngOnInit() {
    setTimeout(() => {
      (<any>$('#cpf')).mask('000.000.000-00');
      (<any>$('#rg')).mask('00.000.000-0');
      (<any>$('#tel-res')).mask('(00) 0000-0000');
      (<any>$('#cellphone')).mask('(00) 0000-00009');
      (<any>$('#zip')).mask('00000-000');
      (<any>$('#pis-pasep')).mask('000.0000.00.0');
      (<any>$('#start-year-m')).mask('0000');
      (<any>$('#conclusion-year-m')).mask('0000');
      (<any>$('#start-year')).mask('0000');
      (<any>$('#conclusion-year')).mask('0000');
      (<any>$('#code-bank')).mask('00000');
    }, 0);

    this._token = this.appService.getUrlParameter("token", location.href)


    if ((this._token == undefined || this._token == '') && !this.appService.isLogged) {
      this.router.navigate(["/access-not-allowed"]);
      return;
    }

    if (this.appService.isLogged) {
      this.pareceristaExterno = JSON.parse(localStorage.getItem("usuarioExterno"));
      this.ufs = this.listarUf();
      this.retornaDadosPareceristaExterno();
      this.emailValido = true;
      return;
    }

    // this.formCadastraParecerista.controls['email'].setValue('');
    // this.email.setValue('');
    // console.log(this.formCadastraParecerista);
    // // this.pareceristaExterno.Email = "";

    this.verificaCadastro();
    this.ufs = this.listarUf();
  }

  onSubmit() {

    if (!this.emailValido)
      return;

    this.cpfValido = this.validaCPF();

    if (!this.cpfValido) {
      this.appService.showToastrWarning(`CPF inválido!`);
      return;
    }

    if (!this.appService.isLogged || this.formCadastraParecerista.value.senha.length > 0){
      this.validarSenha();
    }else
    {
      this.senhaValida = true;
    }

    this.validarDatas();

    if (!this.datasValidadas || !this.senhaValida)
      return;

    this.formCadastraParecerista.controls['token'].setValue(this._token);
    this.formCadastraParecerista.controls['id'].setValue(this.appService.usuarioExterno.Id);

    let body = this.formCadastraParecerista.value;

    var d = new Date();

    if (body.mestradoInicio.length == 4){
      d.setFullYear(body.mestradoInicio);
      body.mestradoInicio = d.toISOString();
    }

    if (body.mestradoConclusao.length == 4){
      d.setFullYear(body.mestradoConclusao);
      body.mestradoConclusao = d.toISOString();
    }

    if (body.doutoradoInicio.length == 4){
      d.setFullYear(body.doutoradoInicio);
      body.doutoradoInicio = d.toISOString();
    }

    if (body.doutoradoConclusao.length == 4){
      d.setFullYear(body.doutoradoConclusao);
      body.doutoradoConclusao = d.toISOString();
    }

    if (this.appService.idExterno == undefined ||
      this.appService.idExterno == '' ||
      this._token == '' ||
      this._token == undefined) {

      this._pareceristaExternoService.alterarDadosCadastrais(body)
        .subscribe(
          res => {

            this.appService.showToastrSuccess(`Dados Cadastrais alterados com sucesso!`);
            this.cadastroConcluido = true;
          },
          err => {
            this.appService.showToastrError(`Não foi possível alterar seus dados cadastrais!`);
          }
        );
    } else {
      this._pareceristaExternoService.completarCadastroPareceristaExterno(body)
        .subscribe(
          res => {

            this.appService.showToastrSuccess(`Cadastro realizado com sucesso !`);
            this.cadastroConcluido = true;
          },
          err => {
            this.appService.showToastrError(`Não foi possível realizar seu cadastro!`);
          }
        );
    }
  }

  retornaDadosPareceristaExterno() {
    this._pareceristaExternoService.retornaPareceristaExterno(this.pareceristaExterno.Id)
      .subscribe(
        res => {
          this.pareceristaExterno = res;
        },
        err => {
          this.appService.showToastrError(`ocorreu um erro inesperado!`);
        }
      );
  }

  validarSenha() {

    if (this.formCadastraParecerista.value.senha.length == 0)
      return;

    if (this.formCadastraParecerista.value.senha.length < 8 || this.formCadastraParecerista.value.senha.length > 20) {
      this.appService.showToastrError(`Sua senha deve conter entre 8 e 20 caracteres!`);
      this.senhaValida = false;
      return;
    }

    if (this.formCadastraParecerista.value.senha != this.formCadastraParecerista.value.confirmarSenha) {
      this.senhaValida = false;
      this.appService.showToastrError(`Sua senha não confere!`);
      return;
    } 

      this.senhaValida = true;
  }

  procuraCep() {

    let _cep = this.formCadastraParecerista.value.cep;

    if (_cep == '' || _cep == undefined)
      return;

    this._pareceristaExternoService.procuraCep(_cep)
      .subscribe(
        res => {
          this.formCadastraParecerista.controls['endereco'].setValue(res.logradouro);
          this.formCadastraParecerista.controls['bairro'].setValue(res.bairro);
          this.formCadastraParecerista.controls['cidade'].setValue(res.localidade);
          this.formCadastraParecerista.controls['estado'].setValue(res.uf);
        },
        err => {
          this.appService.showToastrError(`Erro ao consultar o cep!`);
        }
      );

  }

  verificaCadastro() {
    this._pareceristaExternoService.verificaCadastro(this._token).subscribe((data: any) => {
      this.pareceristaExterno = data;

      if (this.pareceristaExterno == null) {
        this.router.navigate(["/access-not-allowed"]);
        return;
      }

      this.cadastroConcluido = this.pareceristaExterno.StatusCadastro;

      if (!this.cadastroConcluido)
        this.formCadastraParecerista.controls['nome'].setValue(this.pareceristaExterno.Nome);

    }, (error: any) => {
      this.appService.showToastrError(`Ocorreu algum erro inesperado`);
    })
  }

  validarEmailCadastro() {

   if(!this.appService.isLogged && !this.cadastroConcluido){
    if (!this.formCadastraParecerista.get('email').valid!)
    return;

  let body = {
    Token: this._token,
    Email: this.formCadastraParecerista.value.email
  };

  this._pareceristaExternoService.validarEmail(body).subscribe((data: any) => {

    this.emailValido = data;

    if (!this.emailValido)
      this.appService.showToastrError(`E-mail não confere com o cadastrado no sistema!`);

  }, (error: any) => {
    this.appService.showToastrError(`Ocorreu algum erro inesperado`);
  })
   }
  }

  validarDatas() {
    this.validarMestrado();
    this.validarDoutorado();
  }

  validarMestrado() {
    if (Number(this.formCadastraParecerista.value.mestradoInicio) >= Number(this.formCadastraParecerista.value.mestradoConclusao)) {
      this.appService.showToastrWarning(`Ano de início do mestrado não deve ser maior ou igual a conclusão!`);
      this.datasValidadas = false;
      return;
    }
    this.datasValidadas = true;
  }

  validarDoutorado() {

    if (Number(this.formCadastraParecerista.value.doutoradoInicio) >= Number(this.formCadastraParecerista.value.doutoradoConclusao)) {
      this.appService.showToastrWarning(`Ano de início do doutorado não deve ser maior ou igual a conclusão!`);
      this.datasValidadas = false;
      return;
    }
    this.datasValidadas = true;
  }

  listarUf() {
    this._pareceristaExternoService.listarUf().subscribe((data: any) => {

      this.ufs = data;

    }, (error: any) => {
      this.appService.showToastrError(`Ocorreu algum erro inesperado`);
    })
  }

  validaCPF() {
    
    let cpf = this.formCadastraParecerista.value.cpf.replace(/\./gi, "").replace(/\-/gi, "");
    var numeros, digitos, soma, i, resultado, digitos_iguais;
    digitos_iguais = 1;
    if (cpf.length < 11)
      return false;
    for (i = 0; i < cpf.length - 1; i++)

      if (cpf.charAt(i) != cpf.charAt(i + 1)) {
        digitos_iguais = 0;
        break;
      }

    if (!digitos_iguais) {
      numeros = cpf.substring(0, 9);
      digitos = cpf.substring(9);
      soma = 0;
      for (i = 10; i > 1; i--)
        soma += numeros.charAt(10 - i) * i;
      resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
      if (resultado != digitos.charAt(0))
        return false;
      numeros = cpf.substring(0, 10);
      soma = 0;
      for (i = 11; i > 1; i--)
        soma += numeros.charAt(11 - i) * i;
      resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
      if (resultado != digitos.charAt(1))
        return false;
      return true;
    }
    else {
      return false;
    }
  }
}