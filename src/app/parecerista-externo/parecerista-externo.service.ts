import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from "rxjs/operators";
import { baseUrl } from '../shared/shared.variables';
import { AppService } from '../app.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PareceristaExternoService {

  constructor(
    public _http: HttpClient,
    public appService: AppService
  ) { }

  validarEmail(obj): any{
    return this._http.post(`${baseUrl}PareceristaExterno/ValidarEmail`,this.appService.tratarObjeto(obj), this.appService.headerOptions);
  }

  completarCadastroPareceristaExterno(obj): any {
    return this._http.post(`${baseUrl}PareceristaExterno/CadastrarParecerista`, this.appService.tratarObjeto(obj), this.appService.headerOptions);
  }

  verificaCadastro(token): any {
    return this._http.get(`${baseUrl}PareceristaExterno/CadastroConcluido?token=${token}`,this.appService.headerOptions);
  }

  procuraCep(cep): any {
    return this._http.get(`https://viacep.com.br/ws/${cep}/json/`, this.appService.headerOptions);
  }

  listarUf(): any {
    return this._http.get(`${baseUrl}PareceristaExterno/ListarUf`, this.appService.headerOptions);
  }

  retornaPareceristaExterno(id): any {
    return this._http.get(`${baseUrl}PareceristaExterno/RetornaParecerista?id=${id}`, this.appService.headerOptions);
  }

  alterarDadosCadastrais(obj): any {
    return this._http.post(`${baseUrl}PareceristaExterno/AlterarDadosCadastrais`, this.appService.tratarObjeto(obj), this.appService.headerOptions);
  }
  
}